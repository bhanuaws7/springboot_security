package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableWebSecurity
public class TestSpringApplication  extends WebSecurityConfigurerAdapter  {

	public static void main(String[] args) {
		SpringApplication.run(TestSpringApplication.class, args);
	}

	 @Autowired
	 protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		 
		 PasswordEncoder encoder = 
		          PasswordEncoderFactories.createDelegatingPasswordEncoder();
	        auth
	        // enable in memory based authentication with a user named
	        // "user" and "admin"
	        .inMemoryAuthentication().withUser("user").password(encoder.encode("password")).roles("USER").and()
	                        .withUser("admin").password(encoder.encode("bhanu")).roles("USER", "ADMIN");
	    
	        //auth.inMemoryAuthentication().withUser("bhanu").password(encoder.encode("bhanu")).roles("USER").and().withUser("nani").password(encoder.encode("nani")).roles("ADMIN");
	 }
	 
	 @Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		 http
         .csrf().disable()
         .authorizeRequests() 
         .antMatchers("/bhanu").hasRole("ADMIN")
         .antMatchers("/nani").hasRole("USER")
         .anyRequest().authenticated()
         .and()
         .httpBasic()
         .and()
         .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}
