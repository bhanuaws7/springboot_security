package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/welcome")
	public String test() {
		return "welcome to new learing";
	}
	
	@GetMapping("/bhanu")
	public String test1() {
		return "welcome bhanu to new learing";
	}
	
	@GetMapping("/nani")
	public String test2() {
		return "welcome nani to new learing";
	}
	
	
}
